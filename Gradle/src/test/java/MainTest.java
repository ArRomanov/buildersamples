import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.xpath;

public class MainTest extends BaseTest {

    WebDriver gradleDriverOne;

    @BeforeClass
    public void initDriver() {
        initPropertiesForWebDriver();
        gradleDriverOne = getDriver(System.getProperty("browser", "chrome"));
    }

    private WebDriver getDriver(String browser) {
        switch (browser.toLowerCase()) {
            case "firefox":
                return new FirefoxDriver();
            case "chrome":
                return new ChromeDriver();
            default:
                return new ChromeDriver();
        }
    }

    @Test()
    public void firstGradleTest() {

        gradleDriverOne
                .get(MAIN_URL);
        gradleDriverOne
                .findElement(By.id(LOGIN_FIELD))
                .sendKeys("testovyjy");
        gradleDriverOne
                .findElement(By.id(PSWD_FIELD))
                .sendKeys("Password0");
        gradleDriverOne
                .findElement(By.xpath(SUBMIT_BUTTON))
                .click();

        WebDriverWait wait = new WebDriverWait(gradleDriverOne, 10);
        wait.until(
                ExpectedConditions.presenceOfElementLocated(
                        xpath(WRITE_NEW_MAIL_BTN)));
    }

    @AfterClass
    public void closeBrowser() {
        gradleDriverOne.close();
    }
}
