import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.xpath;

public class SecondTest extends BaseTest {

    WebDriver gradleDriverTwo;

    @BeforeClass
    public void initDriver() {
        initPropertiesForWebDriver();
        gradleDriverTwo = new ChromeDriver();
    }

    @Test(enabled = true)
    public void firstGradleTest() {

        gradleDriverTwo
                .get(MAIN_URL);
        gradleDriverTwo
                .findElement(By.id(LOGIN_FIELD))
                .sendKeys("testovyjy");
        gradleDriverTwo
                .findElement(By.id(PSWD_FIELD))
                .sendKeys("Password0");
        gradleDriverTwo
                .findElement(By.xpath(SUBMIT_BUTTON))
                .click();

        WebDriverWait wait = new WebDriverWait(gradleDriverTwo, 10);
        wait.until(
                ExpectedConditions.presenceOfElementLocated(
                        xpath(WRITE_NEW_MAIL_BTN)));
    }

    @AfterClass
    public void closeBrowser() {
        gradleDriverTwo.close();
    }
}
