public class BaseTest {

    static final String MAIN_URL = "https://mail.ru";

    private static String pathToChromeDriverLinux = "src/test/resources/chromedriver";
    private static String pathToChromeDriverWindows = "src/test/resources/chromedriver.exe";
    private static String pathToFirefoxDriverLinux = "src/test/resources/geckodriver";
    private static String pathToFirefoxDriverWindows = "src/test/resources/geckodriver.exe";

    static void initPropertiesForWebDriver() {
        String osName = "";
        osName = System.getProperty("os.name");
        if (osName.contains("Linux")) {
            System.setProperty("webdriver.chrome.driver", pathToChromeDriverLinux);
            System.setProperty("webdriver.gecko.driver", pathToFirefoxDriverLinux);
        } else if (osName.contains("Windows")) {
            System.setProperty("webdriver.chrome.driver", pathToChromeDriverWindows);
            System.setProperty("webdriver.gecko.driver", pathToFirefoxDriverWindows);
        }
    }

    final String LOGIN_FIELD = "mailbox:login";
    final String PSWD_FIELD = "mailbox:password";
    final String SUBMIT_BUTTON = "//input[@value='Войти']";
    final String WRITE_NEW_MAIL_BTN = "//span[text()='Входящие']";
}
