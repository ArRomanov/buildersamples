import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.xpath;

public class SecondTest extends BaseTest {
    WebDriver mavenDriverTwo;

    @BeforeClass
    public void initDriver() {
        initPropertiesForWebDriver();
        mavenDriverTwo = new ChromeDriver();
    }

    @Test(enabled = true)
    public void secondMavenTest() {

        mavenDriverTwo
                .get(MAIN_URL);
        mavenDriverTwo
                .findElement(By.id(LOGIN_FIELD))
                .sendKeys("testovyjy");
        mavenDriverTwo
                .findElement(By.id(PSWD_FIELD))
                .sendKeys("Password0");
        mavenDriverTwo
                .findElement(xpath(SUBMIT_BUTTON))
                .click();

        WebDriverWait wait = new WebDriverWait(mavenDriverTwo, 10);
        wait.until(
                ExpectedConditions.presenceOfElementLocated(
                        xpath(WRITE_NEW_MAIL_BTN)));
    }

    @AfterClass
    public void closeBrowser() {
        mavenDriverTwo.close();
    }
}
