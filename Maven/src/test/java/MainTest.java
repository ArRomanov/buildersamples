import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.xpath;

public class MainTest extends BaseTest {
    WebDriver mavenDriverOne;

    @BeforeClass
    public void initDriver() {
        initPropertiesForWebDriver();
        mavenDriverOne = getDriver(System.getProperty("browser", "chrome"));

    }

    private WebDriver getDriver(String browser) {
        switch (browser) {
            case "firefox":
                return new FirefoxDriver();
            default:
                return new ChromeDriver();
        }
    }

    @Test
    public void firstMavenTest() {

        mavenDriverOne
                .get(MAIN_URL);
        mavenDriverOne
                .findElement(By.id(LOGIN_FIELD))
                .sendKeys("testovyjy");
        mavenDriverOne
                .findElement(By.id(PSWD_FIELD))
                .sendKeys("Password0");
        mavenDriverOne
                .findElement(xpath(SUBMIT_BUTTON))
                .click();

        WebDriverWait wait = new WebDriverWait(mavenDriverOne, 10);
        wait.until(
                ExpectedConditions.presenceOfElementLocated(
                        xpath(WRITE_NEW_MAIL_BTN)));
    }

    @AfterClass
    public void closeBrowser() {
        mavenDriverOne.close();
    }
}
